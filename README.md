# cern-get-sso-cookie

A script to get CERN SSO cookie for use with curl, wget or other command line tools.

Superseded by https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie
